using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Global {
    /// <summary>
    /// Watches chat / image ocr text for common issues and responds accordingly
    /// </summary>
    public class CommonErrorTextPatterns : IGlobalModule {
        private readonly ILogService _log;

        public CommonErrorTextPatterns(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// DirectX issues
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(?=(.*(not initialize|error|crash|problem|virindi).*)).*(direct ?(x|3d)|dx).*")]
        public async Task<SupportMessage> DirextXErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"DirextXErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "Run the game in windowed mode. Press Alt+Enter before entering the game");
            response.AddEmbedFieldText("Tips", "Make sure to install dxwebsetup from <https://files.treestats.net/>");
            response.AddEmbedFieldText("Tips", "Try right clicking `acclient.exe` -> Compatibility -> Check `Disable fullscreen optimizations`");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// vcredist issues
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"((?=.*(error|trouble|problem).*)(?=.*(virindi|vtank|bundle|install).*).*(2005|vcredist).*)|You do not have the visual.*redist.*you must install it|MSVCP70.dll was not found")]
        public async Task<SupportMessage> VCRedistErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"VCRedistErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "Make sure you have installed vcredist_x86 from <https://files.treestats.net/>");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// client closes during character creation
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(can'?t stay logged|finish making char|(make.*char|creat.*char|char.*creat).*(crash|kick|d\/?c|disconnect)|(crash|kick|d\/?c|disconnect).*(make.*char|creat.*char|char.*creat))|client keeps (closing|crashing)")]
        public async Task<SupportMessage> ClientClosesErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"ClientClosesErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "If you are using thwarglauncher advanced mode, either close it during character creation or use simple mode to create characters.");
            response.AddEmbedFieldText("Tips", "Make sure the servername in thwarglauncher exactly matches the server name on the character select screen.");
            response.AddEmbedFieldText("Tips", "Make sure that thwargfilter.dll has been added to decal.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// dat files permissions
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(Can'?t open the data files|Check that they exist and that you have permission to write to them)")]
        public async Task<SupportMessage> DatFilesPermissionsErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"DatFilesPermissionsErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "Try disabling compatibility mode by right clicking acclient.exe in `C:\\Turbine\\Asheron's Call\\`.");
            response.AddEmbedFieldText("Tips", "You may also need to right click and choose properties on each of the .dat files in the same directory. Properties -> Unblock");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// Virindi Tank not fully logged in
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"Please wait until you are fully logged in")]
        public async Task<SupportMessage> VTankNotFullyLoggedInErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"VTankNotFullyLoggedInErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "Try running thwarglauncher as admin. You may also need to check your virindi deps by running the virindi installer and clicking `Check Libs`.");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// Out of memory
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(?=.*(error|failed|crash|issue).*)(out of memory|mem leak)")]
        public async Task<SupportMessage> OutOfMemoryErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"OutOfMemoryErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "Try lowering your graphics settings and disabling any unused decal plugins.");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// missing  gameinfodb.ugd
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"Warning.*added monster .* to DB but species .* has no auto damage definition")]
        public async Task<SupportMessage> MissingGameInfoDBErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"MissingGameInfoDBErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "The virindi server may be having issues. Try downloading [this file](https://cdn.discordapp.com/attachments/714535034291486782/732053855365496892/gameinfodb.ugd) and saving it in `C:\\Games\\VirindiPlugins\\VirindiTank\\`");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// attempting to install virindi bundle without extracting, missing lib
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(Could not load file or assembly|Error).*SharpCode.SharpZip.*")]
        public async Task<SupportMessage> VInstallerSharpZipErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"VInstallerSharpZipErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "Make sure you extract all of the contents of the virindi bundle zip file before installing.");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// black window at launch DualShock4/DualSense5 Drivers
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"black window")]
        public async Task<SupportMessage> BlackWindowErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"BlackWindowErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "If you have DualShock4/DualSense5 drivers installed you will need to uninstall them.  [See here](https://www.reddit.com/r/AsheronsCall/comments/woorha/solved_asherons_call_black_window_not_responsive/)");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// decal not loading blocked plugins.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(fail|unable|error).*register")]
        public async Task<SupportMessage> FailedToRegisterPluginErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"FailedToRegisterPluginErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "You need to unblock the plugin dll by right-clicking on it in windows explorer, and going to Properties — If you're on Windows 11, you'll need to click Show more options first before you can see the Properties option in the context menu. And once you're there, select the General tab and tick Unblock at the bottom in the Security section.\n\n**Make sure you restart decal afterwards**");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// missing VS 2017 runtime
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"140u?\.d[il]{2}")]
        public async Task<SupportMessage> MissingVS2017RuntimeErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"MissingVS2017RuntimeErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "You need to install the VS 2017 runtime from <https://aka.ms/vs/17/release/vc_redist.x86.exe>");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// Failed to save data. Out of disk space? The game will now end.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"failed to save data|out of disk space")]
        public async Task<SupportMessage> FailedToSaveDataErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "CommonErrorTextPatterns", $"FailedToSaveDataErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.AddEmbedFieldText("Tips", "Update your dat / client files by following steps 3 and 4 at <https://emulator.ac/how-to-play/>.");
            response.MessageReference = new MessageReference(message.Id);

            // you can return null here if you dont want to send a response
            return response;
        }
    }
}
