﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Modules {
    public abstract class IGuildModule : IModule {
        public abstract ulong GetGuildID();
    }
}
