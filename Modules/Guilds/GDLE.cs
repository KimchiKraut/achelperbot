﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Guilds {
    /// <summary>
    /// gdle guild specific responses
    /// </summary>
    [DontAutoRegister]
    public class GDLE : IGuildModule {
        private readonly ILogService _log;

        public GDLE(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// Set this to return the guild ID for this modules guild
        /// </summary>
        /// <returns></returns>
        public override ulong GetGuildID() {
            return 380130170855620609;
        }

        /// <summary>
        /// Game Error Failed to establish connection to the server: (Client logon failed) (23000010:04DF9C54
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"(Failed to establish connection to the server|Client logon failed|can'?t connect (to )?server)")]
        public async Task<SupportMessage> ClientLogonFailedChatHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "GDLE", $"ClientLogonFailedChatHandler was triggered in {message?.Channel?.Name}"));

            var supportMessage = new SupportMessage($"");
            supportMessage.AddEmbedFieldText("Tips", "Make sure your username and password is correct, and that you have server type set to `GDLE` in ThwargLauncher", 0);
            if (message.Channel.Id == 380130471750795265) { // reefcull-chat
                supportMessage.AddEmbedFieldText("Tips", "Make sure you've signed up for an account at [Reefcull Account Signup](http://reefcull.gdleac.com/) (no account limit, PVE)", 10);
            }
            else if (message.Channel.Id == 537815648013910016) { // harvestbud-chat
                supportMessage.AddEmbedFieldText("Tips", "Make sure you've signed up for an account at [Harvestbud Account Signup](http://harvestbud.gdleac.com/) (3 account limit, PVE)", 10);
            }
            else {
                supportMessage.AddEmbedFieldText("Tips", "Make sure you've signed up for an account at [Reefcull Account Signup](http://reefcull.gdleac.com/) (no account limit, PVE) or [Harvestbud Account Signup](http://harvestbud.gdleac.com/) (3 account limit, PVE)", 10);
            }

            return supportMessage;
        }
    }
}
