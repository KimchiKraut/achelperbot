﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.DecalExport {
    public class UtilityBelt : IDecalExportModule {
        private ILogService _log;
        private GitlabUpdateCheckService _gitlab;
        private DecalComponent ubFilter;

        public UtilityBelt(ILogService logger, GitlabUpdateCheckService gitlabService) {
            _log = logger;
            _gitlab = gitlabService;
        }

        /// <summary>
        /// Checks if the plugin is loaded
        /// </summary>
        /// <returns></returns>
        private bool CheckExists(Models.DecalExport decalExport) {
            ubFilter = decalExport.Components.Where(p => p.Name == "UtilityBelt").FirstOrDefault();

            return ubFilter != null;
        }

        /// <summary>
        /// Checks that VVS is installed and enabled
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckVVS(IMessage message, Models.DecalExport decalExport) {
            if (!CheckExists(decalExport))
                return null;

            var vvs = decalExport.Services.Where(p => p.Name == "Virindi View Service Bootstrapper").FirstOrDefault();

            if (vvs == null || vvs.Enabled != 1) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", $"UtilityBelt requires Virindi View Service. Make sure it is installed and enabled.");
                return response;
            }

            return null;
        }

        /// <summary>
        /// Checks that VTank is installed and enabled
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckVTank(IMessage message, Models.DecalExport decalExport) {
            if (!CheckExists(decalExport))
                return null;

            var vTank = decalExport.Plugins.Where(p => p.Name == "Virindi Tank").FirstOrDefault();

            if (vTank == null || vTank.Enabled != 1) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", $"UtilityBelt requires the VTank plugin. Make sure it is installed and enabled.");
                return response;
            }

            return null;
        }

        /*
        /// <summary>
        /// Check for latest version
        /// </summary>
        /// <returns></returns>
        [DecalExportRule]
        private async Task<SupportMessage> CheckVersion(IMessage message, Models.DecalExport decalExport) {
            if (!CheckExists(decalExport))
                return null;

            try {
                Version userVersion = new Version(ubFilter.Version.ToString());
                var releaseVersion = _gitlab.GetRepoVersion("UtilityBelt");

                if (userVersion < releaseVersion) {
                    var response = new SupportMessage("");
                    response.AddEmbedFieldText("Warnings", $"UtilityBelt plugin is out of date. Expected:>={releaseVersion} Actual:{userVersion} Grab the latest version at <https://utilitybelt.gitlab.io/>");
                    return response;
                }
            }
            catch (Exception ex) {
                await _log.LogAsync(new LogMessage(LogSeverity.Error, "UtilityBelt", "Unable to check version", ex));
                Console.WriteLine(ex.ToString());
            }
            return null;
        }
        */

        /// <summary>
        /// Checks that dotnet 3.5 is installed
        /// </summary>
        /// <returns></returns>
        [DecalExportRule]
        private async Task<SupportMessage> CheckDotNetVersion(IMessage message, Models.DecalExport decalExport) {
            if (!CheckExists(decalExport))
                return null;

            if (!decalExport.DotNetFrameworks.IsInstalled("3.5")) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", $"UtilityBelt requires dotnet 3.5. Download it [here](https://dotnet.microsoft.com/download/dotnet-framework/net35-sp1)");
                return response;
            }

            return null;
        }
    }
}
