﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.DecalExport {
    public class ThwargFilter : IDecalExportModule {

        private ILogService _log;

        public ThwargFilter(ILogService logger) {
            _log = logger;
        }


        /// <summary>
        /// Check that thwargfilter is registered / enabled.
        /// </summary>
        /// <returns></returns>
        [DecalExportRule]
        private async Task<SupportMessage> CheckIfExists(IMessage message, Models.DecalExport decalExport) {
            var tf = decalExport.NetworkFilters.Where(f => f.Name == "ThwargFilter").FirstOrDefault();

            if (tf == null || tf.Enabled != 1) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Warnings", $"If you are using ThwargLauncher you need to add ThwargFilter.dll to decal. If you are not, you can ignore this message.");
                return response;
            }
            return null;
        }
    }
}
