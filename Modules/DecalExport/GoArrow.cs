﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.DecalExport {
    public class GoArrow : IDecalExportModule {

        private ILogService _log;

        public GoArrow(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// Check that both pre-vvs goarrow and vvs-goarrow are both not enabled
        /// </summary>
        /// <returns></returns>
        [DecalExportRule]
        private async Task<SupportMessage> CheckConflicts(IMessage message, Models.DecalExport decalExport) {
            var originalGoArrow = decalExport.Plugins.Where(f => f.Name == "GoArrow").FirstOrDefault();
            var vvsGoArrow = decalExport.Plugins.Where(f => f.Name == "GoArrow (VVS Edition)").FirstOrDefault();

            if (originalGoArrow != null && vvsGoArrow != null) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Warnings", $"You have both the original GoArrow and GoArrow (VVS Edition) installed. You should remove one of them.");
                return response;
            }

            return null;
        }
    }
}
