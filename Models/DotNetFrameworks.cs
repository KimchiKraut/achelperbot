﻿using ACHelperBot.Lib;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACHelperBot.Models {
    /*
        .NET Frameworks
        [v1.0.3705] (1.0 Final) : Not Installed
        [v1.1.4322] (1.1 Final) : Not Installed
        [v2.0.50215] (2.0 Beta 2)   : Not Installed
        [v2.0.50727] (2.0 Final)    : Installed (Service Pack 2)
        [v3.0] (3.0)        : Installed (Service Pack 2)
        [v3.5] (3.5)        : Installed (Service Pack 1)
        [v4] (4.0)          : Installed (Version: 4.8.03752)
    */
    public class DotNetFrameworks {
        public struct DotNetFramework {
            public string Name { get; private set; }
            public bool Installed { get; private set; }

            public DotNetFramework(string name, bool installed) {
                Name = name;
                Installed = installed;
            }
        }


        /// <summary>
        /// List of installed dotnet frameworks
        /// </summary>
        public List<DotNetFramework> Frameworks { get; private set; } = new List<DotNetFramework>();

        public int Parse(string[] lines, int currentLine) {
            while (currentLine < lines.Length) {
                currentLine++;
                var line = lines[currentLine];

                if (!line.StartsWith("[") || !line.Contains(":"))
                    break;

                (string name, string value) = ParseHelpers.ParseKV(line);

                if (name == null)
                    break;

                if (name.Contains("Beta"))
                    continue;

                var version = name.Split("(")[1].Split(" ")[0].Replace(")","");
                bool installed = value.StartsWith("Installed");

                Frameworks.Add(new DotNetFramework(version, installed));
            }

            return currentLine - 1;
        }

        public void Print() {
            Console.WriteLine(".NET Frameworks");
            foreach (var lib in Frameworks) {
                Console.WriteLine($"\t{lib.Name} : {(lib.Installed ? $"Installed" : "Not Installed")}");
            }
        }

        /// <summary>
        /// Check if the specified d3dx library is installed. ie: HasLibrary("d3dx9_30.dll")
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public bool IsInstalled(string version) {
            return Frameworks.Find(x => x.Name.ToLower() == version).Installed;
        }
    }
}
