﻿using ACHelperBot.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Models {
    public class DecalExportRuleHandler {
        public Type Type { get; }
        public MethodInfo Handler { get; }

        public DecalExportRuleHandler(Type type, MethodInfo handler) {
            Type = type;
            Handler = handler;
        }
    }
}
