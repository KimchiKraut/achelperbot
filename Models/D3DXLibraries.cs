﻿using ACHelperBot.Lib;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACHelperBot.Models {
    /*
        D3DX Libraries (Only d3dx9_30 is required)
        [d3dx9_24.dll]	: Installed (9.5.132.0)
        [d3dx9_25.dll]	: Installed (9.6.168.0)
        [d3dx9_26.dll]	: Installed (9.7.239.0)
        [d3dx9_27.dll]	: Installed (9.8.299.0)
        [d3dx9_28.dll]	: Installed (9.10.455.0)
        [d3dx9_29.dll]	: Installed (9.11.519.0)
        [d3dx9_30.dll]	: Installed (9.12.589.0)
        [d3dx9_31.dll]	: Installed (9.15.779.0)
        [d3dx9_32.dll]	: Installed (9.16.843.0)
        [d3dx9_33.dll]	: Installed (9.18.904.15)
        [d3dx9_34.dll]	: Installed (9.19.949.46)
        [d3dx9_35.dll]	: Installed (9.19.949.1104)
        [d3dx9_36.dll]	: Installed (9.19.949.2111)
        [d3dx9_37.dll]	: Installed (9.22.949.2248)
        [d3dx9_38.dll]	: Installed (9.23.949.2378)
        [d3dx9_39.dll]	: Installed (9.24.949.2307)
        [d3dx9_40.dll]	: Installed (9.24.950.2656)
        [d3dx9_41.dll]	: Installed (9.26.952.2844)
        [d3dx9_42.dll]	: Installed (9.27.952.3001)
        [d3dx9_43.dll]	: Installed (9.29.952.3111)
        [d3dx9_44.dll]	: Not Installed
        [d3dx9_45.dll]	: Not Installed
        [d3dx9_46.dll]	: Not Installed
        [d3dx9_47.dll]	: Not Installed
        [d3dx9_48.dll]	: Not Installed
        [d3dx9_49.dll]	: Not Installed

        // new: 

        D3DX Libraries (Only d3dx9_30 is required)
        [d3dx9_24.dll]	: 9.5.132.0
        [d3dx9_25.dll]	: 9.6.168.0
        [d3dx9_26.dll]	: 9.7.239.0
        [d3dx9_27.dll]	: 9.8.299.0
        [d3dx9_28.dll]	: 9.10.455.0
        [d3dx9_29.dll]	: 9.11.519.0
        [d3dx9_30.dll]	: 9.12.589.0
        [d3dx9_31.dll]	: 9.15.779.0
        [d3dx9_32.dll]	: 9.16.843.0
        [d3dx9_33.dll]	: 9.18.904.15
        [d3dx9_34.dll]	: 9.19.949.46
        [d3dx9_35.dll]	: 9.19.949.1104
        [d3dx9_36.dll]	: 9.19.949.2111
        [d3dx9_37.dll]	: 9.22.949.2248
        [d3dx9_38.dll]	: 9.23.949.2378
        [d3dx9_39.dll]	: 9.24.949.2307
        [d3dx9_40.dll]	: 9.24.950.2656
        [d3dx9_41.dll]	: 9.26.952.2844
        [d3dx9_42.dll]	: 9.27.952.3001
        [d3dx9_43.dll]	: 9.29.952.3111
        [d3dx9_44.dll]	: Not Installed
     */
    public class D3DXLibraries {
        public struct D3DXLibrary {
            public string Name { get; private set; }
            public bool Installed { get; private set; }
            public Version InstalledVersion { get; private set; }

            public D3DXLibrary(string name, bool installed, Version version) {
                Name = name;
                Installed = installed;
                InstalledVersion = version;
            }
        }


        /// <summary>
        /// List of installed d3dx libraries
        /// </summary>
        public List<D3DXLibrary> Libraries { get; private set; } = new List<D3DXLibrary>();

        public int Parse(string[] lines, int currentLine) {
            while (currentLine < lines.Length) {
                currentLine++;
                var line = lines[currentLine];

                if (!line.StartsWith("[") || !line.Contains(":"))
                    break;

                (string name, string value) = ParseHelpers.ParseKV(line);

                if (name == null)
                    break;

                bool installed = !value.Equals("Not Installed");
                Version version = null;
                if (value.StartsWith("Installed")) {
                    version = installed ? new Version(value.Split("(")[1].Replace(")", "")) : new Version("0.0.0.0");
                }
                else {
                    version = installed ? new Version(value) : new Version("0.0.0.0");
                }

                Libraries.Add(new D3DXLibrary(name, installed, version));
            }

            return currentLine - 1;
        }

        public void Print() {
            Console.WriteLine("D3DX Libraries");
            foreach (var lib in Libraries) {
                Console.WriteLine($"\t{lib.Name} : {(lib.Installed ? $"Installed ({lib.InstalledVersion})" : "Not Installed")}");
            }
        }

        /// <summary>
        /// Check if the specified d3dx library is installed. ie: HasLibrary("d3dx9_30.dll")
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public bool IsInstalled(string libraryName) {
            return Libraries.Find(x => x.Name.ToLower() == libraryName).Installed;
        }

        /// <summary>
        /// Returns the version of the specified d3dx library
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public Version GetVersion(string libraryName) {
            return Libraries.Find(x => x.Name.ToLower() == libraryName).InstalledVersion;
        }
    }
}
