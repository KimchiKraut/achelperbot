﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Models {
    public class PluginXmlEntry {
        public string ClassId { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public Version Version {
            get {
                try {
                    return new Version(VersionString);
                }
                catch {
                    return new Version(0, 0, 0, 0);
                }
            }
        }
        public string VersionString { get; set; }
    }
}
