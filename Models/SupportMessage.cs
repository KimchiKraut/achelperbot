﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Models {
    public class SupportMessage {

        public List<EmbedField> EmbedFields = new List<EmbedField>();
        public string Text { get; set; }
        public Color? Color { get; set; }
        public MessageReference MessageReference { get; set; }
        public string EmbedDescription { get; set; }
        public bool Ephemeral { get; set; }

        public Embed Embed {
            get {
                if (EmbedFields.Count == 0)
                    return null;

                EmbedBuilder builder = new EmbedBuilder();
                builder.Color = GetColor();
                builder.Description = EmbedDescription;

                EmbedFields.Sort((a, b) => b.Priority.CompareTo(a.Priority));

                foreach (var field in EmbedFields) {
                    var fieldValue = string.Join(field.LineItemSeperator, field.LineItems.Select(l => l.Text.StartsWith("- ") ? l.Text : $"- {l.Text}").ToArray());


                    if (fieldValue.Length >= 1024) {
                        Console.WriteLine($"TRIMMED LONG FIELD VALUE: (${fieldValue.Length}) ");
                        Console.WriteLine(fieldValue);
                        fieldValue = fieldValue.Substring(0, 1000) + "...";
                    }

                    builder.AddField((fieldBuilder) => {
                        fieldBuilder.IsInline = false;
                        fieldBuilder.Name = field.Title;
                        var separator = field.LineItemSeperator;
                        field.LineItems.Sort();
                        fieldBuilder.Value = fieldValue;
                    });
                }

                return builder.Build();
            }
        }

        public int IssueCount => EmbedFields.Where(f => f.Title != "Out of Date Plugins" && f.Title != "Tips" && f.Title != "Warnings").Count();

        public SupportMessage(string text, string description = null, Color? color = null, bool ephemeral = false, MessageReference messageReference = null) {
            Text = text;
            EmbedDescription = description;
            Color = color;
            Ephemeral = ephemeral;
            MessageReference = messageReference;
        }

        public void AddEmbedFieldText(string title, string text, int priority = 0) {
            var field = EmbedFields.Find(e => e.Title == title);
            var newLineItem = new EmbedLineItem(text);
            if (field == null) {
                var newField = new EmbedField(title, false, priority);
                newField.AddLineItem(newLineItem);
                EmbedFields.Add(newField);
            }
            else {
                field.AddLineItem(newLineItem);
            }
        }

        private Color? GetColor() {
            if (EmbedFields.Any(e => e.Title == "Errors"))
                return Discord.Color.Red;
            else if (EmbedFields.Any(e => e.Title == "Warnings"))
                return Discord.Color.Gold;
            else if (EmbedFields.Any(e => e.Title == "Tips"))
                return Discord.Color.Green;

            return Color ?? Discord.Color.Blue;
        }

        internal void Merge(SupportMessage from) {
            if (from == null)
                return;

            if (!string.IsNullOrEmpty(from.Text)) {
                Text = $"{(Text == null ? "" : $"{Text}\n\n")}{from.Text}";
            }
            if (!string.IsNullOrEmpty(from.EmbedDescription)) {
                EmbedDescription = $"{(EmbedDescription == null ? "" : $"{EmbedDescription}\n\n")}{from.EmbedDescription}";
            }
            if (from.Color != null) {
                Color = from.Color;
            }
            if (from.MessageReference != null) {
                MessageReference = from.MessageReference;
            }

            foreach (var field in from.EmbedFields) {
                var existingField = EmbedFields.Find(e => e.Title == field.Title);
                if (existingField == null) {
                    EmbedFields.Add(field);
                }
                else {
                    existingField.LineItems.AddRange(field.LineItems);
                }
            }
        }
    }
}
