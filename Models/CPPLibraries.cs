﻿
using ACHelperBot.Lib;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACHelperBot.Models {
    /*
        C++ Libraries
        [msvbvm60.dll]  : Installed (6.0.98.15)
        [atl70.dll] : Not Installed
        [mfc70.dll] : Not Installed
        [msvcr70.dll]   : Not Installed
        [msvcp70.dll]   : Not Installed
        [atl71.dll] : Not Installed
        [mfc71.dll] : Not Installed
        [msvcr71.dll]   : Installed (7.10.7031.4)
        [msvcp71.dll]   : Installed (7.10.6052.0)
     */
    class CPPLibraries {
        public struct CPPLibrary {
            public string Name { get; private set; }
            public bool Installed { get; private set; }
            public Version InstalledVersion { get; private set; }

            public CPPLibrary(string name, bool installed, Version version) {
                Name = name;
                Installed = installed;
                InstalledVersion = version;
            }
        }

        /// <summary>
        /// List of installed C++ libraries
        /// </summary>
        public List<CPPLibrary> Libraries { get; private set; } = new List<CPPLibrary>();

        public int Parse(string[] lines, int currentLine) {
            while (currentLine < lines.Length) {
                currentLine++;
                var line = lines[currentLine];

                if (!line.StartsWith("[") || !line.Contains(":"))
                    break;

                (string name, string value) = ParseHelpers.ParseKV(line);

                if (name == null)
                    break;

                bool installed = value.StartsWith("Installed");
                Version version = installed ? new Version(value.Split("(")[1].Replace(")", "")) : new Version("0.0.0.0");

                Libraries.Add(new CPPLibrary(name, installed, version));
            }

            return currentLine - 1;
        }

        public void Print() {
            Console.WriteLine("C++ Libraries");
            foreach (var lib in Libraries) {
                Console.WriteLine($"\t{lib.Name} : {(lib.Installed ? $"Installed ({lib.InstalledVersion})" : "Not Installed")}");
            }
        }

        /// <summary>
        /// Check if the specified C++ library is installed. ie: HasLibrary("d3dx9_30.dll")
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public bool IsInstalled(string libraryName) {
            return Libraries.Find(x => x.Name.ToLower() == libraryName).Installed;
        }

        /// <summary>
        /// Returns the version of the specified C++ library
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public Version GetVersion(string libraryName) {
            return Libraries.Find(x => x.Name.ToLower() == libraryName).InstalledVersion;
        }
    }
}
