﻿using ACHelperBot.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHelperBot.Models {
    /*
        Decal Settings
        [Injection Method] : CBT Hook (Alternate)
        [Portal Path]	: C:\Turbine\Asheron's Call
        [Launcher App]	: acclient.exe
        [Update URL]	: http://update.decaldev.com
        [memlocs.xml]	: 27376 bytes, Version 0.0.11.6096
        [messages.xml]	: 55090 bytes, Version 2013.07.27.0
        [Video Memory]	: -65536


    // new: 

        [Injection Method]	: Timer (Default)
        [Dual Log]	: Disabled
        [No Movies]	: Disabled
        [No Splash]	: Disabled
        [Disable Old UI]	: Enabled
        [Portal Path]	: C:\Turbine\Asheron's Call
        [Launcher App]	: acclient.exe
        [Update URL]	: http://update.decaldev.com
        [memlocs.xml]	: 27376 bytes, Version 0.0.11.6096
        [messages.xml]	: 55090 bytes, Version 2013.07.27.0
        [Video Memory]	: -65536
    */

    public class DecalSettings {
        public enum InjectionMethodType {
            Timer,
            CBTHook
        }

        /// <summary>
        /// Decal Injection method, can be timer or cbthook
        /// </summary>
        public InjectionMethodType InjectionMethod { get; private set; }

        /// <summary>
        /// Path to ac client files. default: C:\Turbine\Asheron's Call
        /// </summary>
        public string PortalPath { get; private set; }

        /// <summary>
        /// relative path to ac client, defaults to acclient.exe
        /// </summary>
        public string LauncherApp { get; private set; }

        /// <summary>
        /// ac client version
        /// </summary>
        public Version ClientVersion { get; private set; } = new Version("0.0.0.0");

        /// <summary>
        /// Decal update URL, defaults to http://update.decaldev.com
        /// </summary>
        public string UpdateURL { get; private set; }

        /// <summary>
        /// Size of memlocs file in bytes
        /// </summary>
        public int MemlocsXMLSize { get; private set; }

        /// <summary>
        /// Memlocs file version
        /// </summary>
        public Version MemlocsXMLVersion { get; private set; } = new Version("0.0.0.0");

        /// <summary>
        /// Messages.xml file size
        /// </summary>
        public int MessagesXMLSize { get; private set; }

        /// <summary>
        /// Messages.xml file version
        /// </summary>
        public Version MessagesXMLVersion { get; private set; } = new Version("0.0.0.0");

        /// <summary>
        /// Video memory in bytes? Mine was -65536
        /// </summary>
        public int VideoMemory { get; private set; }

        /// <summary>
        /// Dual log setting
        /// </summary>
        public bool DualLogEnabled { get; private set; } = true;

        /// <summary>
        /// NoMovies Enabled
        /// </summary>
        public bool NoMoviesEnabled { get; private set; } = true;

        /// <summary>
        /// NoSplash Enabled
        /// </summary>
        public bool NoSplashEnabled { get; private set; } = true;

        /// <summary>
        /// OldUI Enabled
        /// </summary>
        public bool OldUIEnabled { get; private set; } = true;

        public void Print() {
            Console.WriteLine("Decal Settings:");
            Console.WriteLine($"\tInjection Method: {InjectionMethod}");
            Console.WriteLine($"\tPortal Path: {PortalPath}");
            Console.WriteLine($"\tLauncher App: {LauncherApp}");
            Console.WriteLine($"\tUpdate URL: {UpdateURL}");
            Console.WriteLine($"\tDual Log: {UpdateURL}");
            Console.WriteLine($"\tNo Movies: {NoMoviesEnabled}");
            Console.WriteLine($"\tNo Splash: {NoSplashEnabled}");
            Console.WriteLine($"\tDisable Old UI: {OldUIEnabled}");
            Console.WriteLine($"\tMemlocs XML Size: {MemlocsXMLSize}");
            Console.WriteLine($"\tMemlocs XML Version: {MemlocsXMLVersion}");
            Console.WriteLine($"\tMessages XML Size: {MessagesXMLSize}");
            Console.WriteLine($"\tMessages XML Version: {MessagesXMLVersion}");
        }

        internal int Parse(string[] lines, int currentLine) {
            while (currentLine < lines.Length) {
                currentLine++;
                var line = lines[currentLine];

                if (!line.StartsWith("[") || !line.Contains(":"))
                    break;

                (string key, string value) = ParseHelpers.ParseKV(line);

                switch (key) {
                    case "Injection Method":
                        InjectionMethod = value.Contains("Timer") ? InjectionMethodType.Timer : InjectionMethodType.CBTHook;
                        break;

                    case "Portal Path":
                        PortalPath = value;
                        break;

                    case "Launcher App":
                        var parts = value.Split(", ");
                        if (parts.Length == 2) {
                            LauncherApp = parts[0];
                            try {
                                ClientVersion = new Version(parts[1]);
                            }
                            catch { }
                        }
                        else {
                            LauncherApp = value;
                        }
                        break;

                    case "Update URL":
                        UpdateURL = value;
                        break;

                    case "Dual Log":
                        DualLogEnabled = value.Contains("Enabled") ? true : false;
                        break;

                    case "No Movies":
                        NoMoviesEnabled = value.Contains("Enabled") ? true : false;
                        break;

                    case "No Splash":
                        NoSplashEnabled = value.Contains("Enabled") ? true : false;
                        break;

                    case "Disable Old UI":
                        OldUIEnabled = value.Contains("Enabled") ? false : true;
                        break;

                    case "memlocs.xml":
                        var memlocParts = value.Split(" bytes, Version ");
                        if (memlocParts.Length == 2) {
                            if (Int32.TryParse(memlocParts[0], out int memlocBytes))
                                MemlocsXMLSize = memlocBytes;
                            MemlocsXMLVersion = new Version(memlocParts[1]);
                        }
                        break;

                    case "messages.xml":
                        var messageParts = value.Split(" bytes, Version ");
                        if (messageParts.Length == 2) {
                            if (Int32.TryParse(messageParts[0], out int messageBytes))
                                MessagesXMLSize = messageBytes;
                            MessagesXMLVersion = new Version(messageParts[1]);
                        }
                        break;

                    case "Video Memory":
                        if (Int32.TryParse(value, out int videoMemory))
                            VideoMemory = videoMemory;
                        break;

                    default:
                        Console.Write($"Found weird decal setting: {key} : {value}\n");
                        break;
                }
            }

            return currentLine - 1;
        }
    }
}
