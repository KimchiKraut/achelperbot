﻿using System;
using Discord;
using Discord.Net;
using Discord.Commands;
using Discord.Interactions;
using Discord.WebSocket;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using Microsoft.Extensions.Configuration;
using ACHelperBot.Services;

namespace ACHelperBot {
    class Program {
        private readonly IConfiguration _config;
        private ILogService _logger;
        private DiscordSocketClient _client;
        private InteractionService _commands;
        public static Task Main(string[] args) => new Program().MainAsync();

        public Program() {  
            _config = new ConfigurationService();
            _logger = new BotLogService();

            var socketConfig = new DiscordSocketConfig();
            socketConfig.GatewayIntents = GatewayIntents.DirectMessages | GatewayIntents.GuildMessages | GatewayIntents.Guilds | GatewayIntents.GuildMembers | GatewayIntents.MessageContent;
            socketConfig.AlwaysDownloadUsers = false;
            socketConfig.AlwaysResolveStickers = false;
            socketConfig.LogLevel = LogSeverity.Info;

            _client = new DiscordSocketClient(socketConfig);
        }
        
        public async Task MainAsync() {
            using (var services = ConfigureServices()) {
                _ = services.GetRequiredService<DecalUpdateCheckService>();
                _ = services.GetRequiredService<GitlabUpdateCheckService>();
                _ = services.GetRequiredService<BotActivityService>();
                _ = services.GetRequiredService<ModuleService>();
                _ = services.GetRequiredService<DecalExportService>();
                _ = services.GetRequiredService<UBScriptService>();
                _ = services.GetRequiredService<WebService>();
                _commands = services.GetRequiredService<InteractionService>();
                //_ = services.GetRequiredService<OpenAIGPT3Service>();

                _client.Log += _logger.LogAsync;
                _client.Ready += ReadyAsync;
                _commands.Log += _logger.LogAsync;
                _client.Disconnected += _client_Disconnected;

                await _client.LoginAsync(TokenType.Bot, _config["DISCORD_TOKEN"]);
                await _client.StartAsync();

                await services.GetRequiredService<CommandHandlerService>().InitializeAsync();

                await Task.Delay(Timeout.Infinite);
            }
        }

        private Task _client_Disconnected(Exception arg) {
            Environment.Exit(0);
            return Task.CompletedTask;
        }

        private async Task ReadyAsync() {
            await _logger.LogAsync(new LogMessage(LogSeverity.Info, "Program", $"Connected as -> [{_client.CurrentUser}]"));
        }

        private ServiceProvider ConfigureServices() {
            var collection = new ServiceCollection()
                .AddSingleton(_config)
                .AddSingleton(_client)
                .AddSingleton(_logger)
                .AddSingleton<InteractionService>()
                .AddSingleton<BotActivityService>()
                .AddSingleton<ModuleService>()
                .AddSingleton<ImageTextService>()
                .AddSingleton<DecalExportService>()
                .AddSingleton<CommandHandlerService>()
                .AddSingleton<DecalUpdateCheckService>()
                .AddSingleton<UBScriptService>()
                .AddSingleton<GitlabUpdateCheckService>()
                .AddSingleton<WebService>();
            //.AddSingleton<OpenAIGPT3Service>();

            var moduleTypes = ModuleService.GetModuleTypes();
            foreach (var type in moduleTypes) {
                collection.AddTransient(type);
            }
                
            return collection.BuildServiceProvider();
        }
    }
}
