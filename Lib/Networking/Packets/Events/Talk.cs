﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ACClientLib.Lib.Networking.Packets.Events {
    class TalkEvent : EventFragment {
        public string Text;

        public TalkEvent(string text) : base(0x0015) {
            Text = text;
        }

        protected override void OnSerialize(BinaryWriter writer) {
            base.OnSerialize(writer);
            writer.WriteString16(Text);
        }
    }
}
