﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ACClientLib.Lib.Networking.Constants;

namespace ACClientLib.Lib.Networking.Packets.Events {
    class ChangeCombatModeEvent : EventFragment {
        public CombatState CombatState;

        public ChangeCombatModeEvent(CombatState combatState) : base(0x0053) {
            CombatState = combatState;
        }

        protected override void OnSerialize(BinaryWriter writer) {
            base.OnSerialize(writer);
            writer.Write((uint)CombatState);
        }
    }
}
