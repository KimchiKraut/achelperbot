using System;
using System.Buffers;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using ACClientLib.Lib.Networking.Crypto;

namespace ACClientLib.Lib.Networking.Packets {
    public class Fragment {
        public FragmentHeader Header;

        public uint Type { get; set; }

        public Fragment() {
        }

        public Fragment(uint type)
            : this() {
            Type = type;
        }

        public Fragment(uint type, ushort group)
            : this(type) {
            Header.Group = group;
        }

        public Fragment(uint type, FragmentGroup group)
            : this(type, (ushort)group) {
        }

        public uint Hash(Span<byte> buffer) {
            Span<FragmentHeader> pHeader = MemoryMarshal.Cast<byte, FragmentHeader>(buffer);
            ref FragmentHeader header = ref MemoryMarshal.GetReference(pHeader);

            int len = header.Size - FragmentHeader.SizeOf;

            uint result = Checksum.GetMagicNumber(buffer, FragmentHeader.SizeOf, true);
            result += Checksum.GetMagicNumber(
                buffer.Slice(FragmentHeader.SizeOf, len), len, true);

            return result;
        }

        public byte[] ToMessageBytes() {
            using (var stream = new MemoryStream())
            using (BinaryWriter writer = new BinaryWriter(stream, System.Text.Encoding.ASCII, true)) {
                writer.Write(Type);
                OnSerialize(writer);
                return stream.ToArray();
            }
        }

        public int Serialize(Stream stream) {
            int start = (int)stream.Position;

            stream.Seek(FragmentHeader.SizeOf, SeekOrigin.Current);

            // write extended data to stream
            using (BinaryWriter writer = new BinaryWriter(stream, System.Text.Encoding.ASCII, true)) {
                writer.Write(Type);

                OnSerialize(writer);
            }

            int resultLength = (int)stream.Position - start;

            Header.Size = (ushort)resultLength;
            stream.Seek(start, SeekOrigin.Begin);

            // write header to stream
            var pBytes = ByteHelpers.StructureToByteArray(ref Header);
            stream.Write(pBytes, 0, pBytes.Length);

            stream.Seek(resultLength - FragmentHeader.SizeOf, SeekOrigin.Current);

            return resultLength;
        }

        protected virtual void OnSerialize(BinaryWriter writer) {
        }

    }
}
