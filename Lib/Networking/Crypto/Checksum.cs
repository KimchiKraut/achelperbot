using System;
using System.Runtime.InteropServices;

namespace ACClientLib.Lib.Networking.Crypto {
    internal static class Checksum {
        public static uint GetMagicNumber<T>(ref T packet, int size, bool includeSize) where T : struct {
            Span<byte> bytes = new Span<byte>(ByteHelpers.StructureToByteArray(ref packet));
            return GetMagicNumber(bytes, size, includeSize);
        }

        public static uint GetMagicNumber(in Span<byte> pBuffer, int size, bool includeSize) {
            uint magic = 0;

            if (includeSize)
                magic += (uint)size << 16;

            int i = 0;

            //uint* pInts = (uint*)pBuffer;
            Span<uint> pInts = MemoryMarshal.Cast<byte, uint>(pBuffer);
            for (i = 0; i < size / 4; i++)
                magic += pInts[i];

            int shift = 3;
            for (i = i * 4; i < size; i++) {
                magic += (uint)pBuffer[i] << shift * 8;
                shift--;
            }

            return magic;
        }

        public static unsafe uint Calculate(byte[] buffer) {
            uint checksum1 = 0;
            uint checksum2 = 0;

            Span<byte> pBuffer = buffer;
            Span<TransitHeader> pHeader = MemoryMarshal.Cast<byte, TransitHeader>(pBuffer);
            ref TransitHeader header = ref MemoryMarshal.GetReference(pHeader);

            header.Checksum = 0xbadd70dd;
            checksum1 = GetMagicNumber(pBuffer, TransitHeader.SizeOf, true);
            checksum2 = GetMagicNumber(pBuffer.Slice(TransitHeader.SizeOf), header.Size, true);
            header.Checksum = checksum1 + checksum2;

            return checksum1 + checksum2;
        }

        public static unsafe uint Calculate200(byte[] buffer) {
            uint checksum = 0;
            Span<byte> pBuffer = buffer;
            Span<TransitHeader> pHeader = MemoryMarshal.Cast<byte, TransitHeader>(pBuffer);
            ref TransitHeader header = ref MemoryMarshal.GetReference(pHeader);

            int pos = TransitHeader.SizeOf;
            int end = TransitHeader.SizeOf + header.Size;

            // TODO: optional header data. fragments may not start here

            while (pos < end) {
                Span<byte> pFrag = pBuffer.Slice(pos, FragmentHeader.SizeOf);
                Span<FragmentHeader> pFragHeader = MemoryMarshal.Cast<byte, FragmentHeader>(pFrag);
                ref FragmentHeader fragHeader = ref MemoryMarshal.GetReference(pFragHeader);

                int len = fragHeader.Size;
                checksum += GetMagicNumber(pFrag, FragmentHeader.SizeOf, true);
                checksum += GetMagicNumber(pBuffer.Slice(pos + FragmentHeader.SizeOf), len - FragmentHeader.SizeOf, true);

                pos += len;
            }

            // fixed (byte* pBuffer = buffer)
            // {
            // 	byte* pEnd = pBuffer + ((Net.TransitHeader*)pBuffer)->Size + HeaderSize;

            // 	for (byte* pFrag = pBuffer + HeaderSize; pFrag < pEnd; )
            // 	{
            // 		int len = ((Net.FragmentHeader*)pFrag)->Size;
            // 		checksum += GetMagicNumber(pFrag, FragmentHeaderSize, true);
            // 		checksum += GetMagicNumber(pFrag + FragmentHeaderSize, len - FragmentHeaderSize, true);
            // 		pFrag += len;
            // 	}
            // }

            return checksum;
        }

        public static uint CalculateTransport(byte[] buffer) {
            uint checksum = 0;
            Span<byte> pBuffer = buffer;
            Span<TransitHeader> headerSpan = MemoryMarshal.Cast<byte, TransitHeader>(pBuffer);
            ref TransitHeader header = ref MemoryMarshal.GetReference(headerSpan);

            uint orig = header.Checksum;
            header.Checksum = 0xbadd70dd;
            checksum += GetMagicNumber(pBuffer, TransitHeader.SizeOf, true);
            header.Checksum = orig;

            return checksum;
        }
    }
}
