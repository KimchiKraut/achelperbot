﻿using ACClientLib.Lib.Networking.Packets;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ACClientLib.Lib.Networking.Transports {
    public class UDPTransport : INetworkTransport {
        private Thread _networkThread;
        private Socket _connection;

        private IPEndPoint localAddr;
        private bool _shouldBeRunning;

        public string Host { get; private set; }
        public int Port { get; private set; }

        public override event EventHandler<TransportDataEventArgs> OnData;

        private ConcurrentQueue<TransportDataEventArgs> _incomingDataQueue = new ConcurrentQueue<TransportDataEventArgs>();
        private ConcurrentQueue<Action> _actionQueue = new ConcurrentQueue<Action>();

        public UDPTransport() {
        }

        public override void Start(string host, int port) {
            Host = host;
            Port = port;

            // todo: can we bind to an unsused port?
            var r = new Random();
            var bindPort = r.Next(10000, 15000);
            localAddr = new IPEndPoint(IPAddress.Loopback, bindPort);

            _networkThread = new Thread(NetworkThreadStart);
            _networkThread.Start();
        }

        public override void Update() {
            while (_incomingDataQueue.TryDequeue(out var evt)) {
                OnData?.Invoke(this, evt);
            }
        }

        public override void Send(ServerInfo si, byte[] data, int length, bool useRead) {
            _actionQueue.Enqueue(() => {
                try {
                    System.Diagnostics.Debug.WriteLine($"Send: {useRead} {(useRead ? si.ReadAddress : si.WriteAddress)}");
                    _connection.SendTo(data, length, SocketFlags.None, useRead ? si.ReadAddress : si.WriteAddress);
                }
                finally {
                    ArrayPool<byte>.Shared.Return(data, true);
                }
            });
        }

        private void DoReceive() {
            IPEndPoint ip = new IPEndPoint(IPAddress.Any, 0);
            EndPoint ep = ip;

            byte[] buffer = ArrayPool<byte>.Shared.Rent(2048);

            _connection.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref ep, OnReceive, buffer);
        }

        private void OnReceive(IAsyncResult result) {
            if (!result.IsCompleted)
                if (!result.AsyncWaitHandle.WaitOne(10000))
                    throw new ApplicationException("Network connection failure");

            IPEndPoint ip = new IPEndPoint(IPAddress.Any, 0);
            EndPoint ep = ip;

            try {
                int length = _connection.EndReceiveFrom(result, ref ep);
                byte[] buffer = (byte[])result.AsyncState;

                if (_shouldBeRunning)
                    DoReceive();

                ip = (IPEndPoint)ep;
                //Console.WriteLine($"Data from {ip.Address.ToString()}:{ip.Port}");
                _incomingDataQueue.Enqueue(new TransportDataEventArgs(ip, buffer));
            }
            catch (SocketException er) {
                // disconnected?
                Console.WriteLine(er);
            }
        }

        private async void NetworkThreadStart() {
            Console.WriteLine("Network Thread Start");

            _connection = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            Console.WriteLine($"Binding to local address: {localAddr}");

            System.Diagnostics.Debug.WriteLine($"Binding to local address: {localAddr}");

            _connection.Bind(this.localAddr);
            _shouldBeRunning = true;

            System.Diagnostics.Debug.WriteLine($"Before DoReceive");
            DoReceive();
            System.Diagnostics.Debug.WriteLine($"After DoReceive");

            try {
                while (_shouldBeRunning) {
                    while (_actionQueue.TryDequeue(out var action)) {
                        action?.Invoke();
                    }
                    if (!Thread.Yield()) {
                        await Task.Delay(10);
                    }
                }
            }
            catch (ThreadAbortException) {
            }
        }

        public void Dispose() {
            _shouldBeRunning = false;
            _networkThread?.Abort();
        }
    }
}
