﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Lib {
    public class ScriptLogger : ILogger {

        private class LogScope : IDisposable {
            public void Dispose() {

            }
        }

        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            return new LogScope();   
        }

        public bool IsEnabled(LogLevel logLevel) {
            return logLevel >= LogLevel.Information;
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            if (IsEnabled(logLevel))
                Console.WriteLine($"{message}");
        }

        public void Log(Exception ex) {
            Log($"Exception: \n{ex}", LogLevel.Error);
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {

            Log(formatter(state, exception), logLevel);
        }
    }
}
