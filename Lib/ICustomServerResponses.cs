﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Lib {
    abstract class ICustomServerResponses {
        public abstract List<string> GetResponses(SocketMessage message);
        public virtual async Task RegisterSlashCommands(DiscordSocketClient client) {

        }

        internal virtual async Task SlashCommandHandler(SocketSlashCommand command) {

        }

        internal virtual async Task UserContextCommandHandler(SocketUserCommand command) {

        }

        internal virtual async Task MessageContextCommandHandler(SocketMessageCommand command) {

        }

        internal virtual async Task<List<ApplicationCommandProperties>> GetGuildContextCommands(DiscordSocketClient client) {
            return new List<ApplicationCommandProperties>();
        }

        public abstract ulong GetGuildId();
    }
}
