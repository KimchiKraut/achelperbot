﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Newtonsoft.Json;
using ACE.DatLoader;
using static System.Collections.Specialized.BitVector32;
using UtilityBelt.Scripting;
using Microsoft.Extensions.Logging;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Enums;
using WattleScript.Interpreter;

namespace ACHelperBot.Lib {
    public class ScriptsController {
        private JsonSerializerSettings settings;
        private ScriptManagerOptions options;
        private ScriptLogger scriptLogger;
        private List<InitAction> initActions = new List<InitAction>();

        public static PortalDatDatabase PortalDat { get; private set; }
        public static CellDatDatabase CellDat { get; private set; }
        public static LanguageDatDatabase LanguageDat { get; private set; }
        public ScriptManager Manager { get; private set; }

        private class InitAction {
            public string Name { get; set; }

            public Action Action { get; set; }
        }
        private class MessageData {
            public enum MessageDirection {
                In,
                Out
            }
            public byte[] Data;
            public MessageDirection Direction;
        }

        public ScriptsController() {
            settings = new JsonSerializerSettings() {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                ContractResolver = new NonPublicPropertiesResolver()
            };

            options = new ScriptManagerOptions() {
                StartVSCodeDebugServer = false
            };

            scriptLogger = new ScriptLogger();
            
            initActions.Add(new InitAction() {
                Action = DeserializePortalDat,
                Name = "Deserializing portal.dat.json.gz"
            });
            initActions.Add(new InitAction() {
                Action = DeserializeCellDat,
                Name = "Deserializing cell.dat.json.gz"
            });
            initActions.Add(new InitAction() {
                Action = DeserializeLanguageDat,
                Name = "Deserializing language.dat.json.gz"
            });
            initActions.Add(new InitAction() {
                Action = InitScriptManager,
                Name = "Initializing script manager"
            });
            initActions.Add(new InitAction() {
                Action = InitGameState,
                Name = "Reticulating weenies"
            });

            while (initActions.Count > 0) {
                initActions[0].Action.Invoke();
                initActions.RemoveAt(0);
            }
        }

        private void InitScriptManager() {
            var options = new ScriptManagerOptions() {
                ScriptDirectory = "scripts",
                StartVSCodeDebugServer = false,
                ScriptDataDirectory = "scriptdata"
            };

            Manager = new UtilityBelt.Scripting.ScriptManager(options);

            scriptLogger = new ScriptLogger();

            Manager.RegisterComponent(typeof(ILogger), typeof(ScriptLogger), true, scriptLogger);
            Manager.RegisterComponent(typeof(IClientActionsRaw), typeof(FakeClientActions), true, new FakeClientActions(scriptLogger));
            Manager.RegisterComponent(typeof(PortalDatDatabase), typeof(PortalDatDatabase), true, PortalDat);
            Manager.RegisterComponent(typeof(CellDatDatabase), typeof(CellDatDatabase), true, CellDat);
            Manager.RegisterComponent(typeof(LanguageDatDatabase), typeof(LanguageDatDatabase), true, LanguageDat);


            Manager.Initialize((s) => {
                Console.WriteLine(s);
            });
        }

        private void InitGameState() {
            using (Stream manifestResourceStream = typeof(Program).Assembly.GetManifestResourceStream("ACHelperBot.Assets.gamestate.json.gz")) {
                var bytes = new byte[manifestResourceStream.Length];
                manifestResourceStream.Read(bytes, 0, bytes.Length);
                string json = Decompress(bytes);
                var messages = JsonConvert.DeserializeObject<List<MessageData>>(json);

                foreach (var message in messages) {
                    if (message.Direction == MessageData.MessageDirection.Out) {
                        Manager.HandleOutgoing(message.Data);
                    }
                    else {
                        Manager.HandleIncoming(message.Data);
                    }
                }
            }
        }

        private void DeserializePortalDat() {
            using (Stream manifestResourceStream = typeof(Program).Assembly.GetManifestResourceStream("ACHelperBot.Assets.portaldat.json.gz")) {
                var bytes = new byte[manifestResourceStream.Length];
                manifestResourceStream.Read(bytes, 0, bytes.Length);
                string json = Decompress(bytes);
                var cache = JsonConvert.DeserializeObject<Dictionary<uint, ACE.DatLoader.FileTypes.FileType>>(json, settings);
                PortalDat = new PortalDatDatabase(cache);
            }
        }
        private void DeserializeCellDat() {
            using (Stream manifestResourceStream = typeof(Program).Assembly.GetManifestResourceStream("ACHelperBot.Assets.celldat.json.gz")) {
                var bytes = new byte[manifestResourceStream.Length];
                manifestResourceStream.Read(bytes, 0, bytes.Length);
                string json = Decompress(bytes);
                var cache = JsonConvert.DeserializeObject<Dictionary<uint, ACE.DatLoader.FileTypes.FileType>>(json, settings);
                CellDat = new CellDatDatabase(cache);
            }
        }

        private void DeserializeLanguageDat() {
            using (Stream manifestResourceStream = typeof(Program).Assembly.GetManifestResourceStream("ACHelperBot.Assets.languagedat.json.gz")) {
                var bytes = new byte[manifestResourceStream.Length];
                manifestResourceStream.Read(bytes, 0, bytes.Length);
                string json = Decompress(bytes);
                var cache = JsonConvert.DeserializeObject<Dictionary<uint, ACE.DatLoader.FileTypes.FileType>>(json, settings);
                LanguageDat = new LanguageDatDatabase(cache);
            }
        }

        public static void save(Stream source, Stream destination) {
            byte[] bytes = new byte[1024 * 1024 * 10];

            int count;

            while ((count = source.Read(bytes, 0, bytes.Length)) != 0) {
                destination.Write(bytes, 0, count);
            }
        }

        public static string Decompress(byte[] bytes) {
            using (var msi = new MemoryStream(bytes))
            using (var memoryStream = new MemoryStream()) {
                using (var gZipStream = new GZipStream(msi, CompressionMode.Decompress)) {
                    save(gZipStream, memoryStream);
                }

                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }
    }
}
