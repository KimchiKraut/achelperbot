﻿using ACHelperBot.Lib;
using ACHelperBot.Models;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Services {
    /// <summary>
    /// Monitors links and attachments for decal exports
    /// </summary>
    public class DecalExportService {
        private static Regex URLRegex = new Regex(@"(https?:\/\/)?([\w\-])+\.{1}([a-zA-Z]{2,63})([\/\w-]*)*\/?\??([^#\n\r]*)?#?([^\n\r]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static Regex PasteBinPathRegex = new Regex(@"^/[a-z0-9]+$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        private readonly ILogService _logger;
        private IConfiguration _config;
        private DiscordSocketClient _client;
        private ModuleService _modules;
        private IServiceProvider _services;

        public DecalExportService(ILogService logger, IConfiguration config, DiscordSocketClient client, ModuleService modules, IServiceProvider services) {
            _logger = logger;
            _config = config;
            _client = client;
            _modules = modules;
            _services = services;

            _client.MessageReceived += Client_MessageReceived;
        }

        private Task Client_MessageReceived(SocketMessage message) {
            if (message.Author == null || message.Channel == null || message.Author.IsBot || !(message.Channel is SocketGuildChannel guildChannel))
                return Task.CompletedTask;

            _ = Task.Run(async () => {
                try {
                    var exports = await GetDecalExportResponses(message);

                    foreach (var export in exports) {
                        var response = export.Response;
                        await message.Channel.SendMessageAsync(response.Text, false, response.Embed, null, null, new MessageReference(message.Id));
                    }
                }
                catch (Exception ex) {
                    await _logger.LogAsync(new LogMessage(LogSeverity.Error, "DecalExportService", $"Error running DecalExportHandlers", ex));
                }
            });

            return Task.CompletedTask;
        }

        public async Task<List<DecalExport>> GetDecalExportResponses(IMessage arg) {
            var decalExportLinks = FindPossibleDecalExportLinks(arg);
            var decalExportAttachments = FindPossibleDecalExportAttachments(arg);
            var possibleDecalExportLinks = decalExportLinks.Concat(decalExportAttachments);

            if (possibleDecalExportLinks.Count() == 0) {
                return new List<DecalExport>();
            }

            return await DownloadAndCheckDecalExports(arg, possibleDecalExportLinks);
        }

        /// <summary>
        /// Returns all attachment urls on a message that could be decal exports
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        internal List<string> FindPossibleDecalExportAttachments(IMessage message) {
            List<string> results = new List<string>();

            foreach (var attachment in message.Attachments) {
                // only looking at txt files and files with no extension
                if (!attachment.Filename.EndsWith(".txt") && attachment.Filename.Contains("."))
                    continue;

                // skip files larger than 100k (my decal export is ~15k with several plugins)
                if (attachment.Size > 1024 * 1024 * 100)
                    continue;

                results.Add(attachment.Url);
            }

            return results;
        }

        /// <summary>
        /// Find any links in the message and return a list of possible decal export links
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        internal static List<string> FindPossibleDecalExportLinks(IMessage arg) {
            List<string> results = new List<string>();
            MatchCollection matches = URLRegex.Matches(arg.ToString());
            foreach (Match match in matches) {
                try {
                    Uri foundUri = new Uri(match.Value);

                    // currently only looking for pastebin
                    if (foundUri.Host.ToLower() == "pastebin.com" && PasteBinPathRegex.IsMatch(foundUri.AbsolutePath)) {
                        results.Add($"https://pastebin.com/raw{foundUri.AbsolutePath}");
                    }
                    else if (match.Value.EndsWith(".txt")) {
                        results.Add(match.Value);
                    }
                }
                catch { }
            }

            return results;
        }

        public async Task<List<DecalExport>> DownloadAndCheckDecalExports(IMessage message, IEnumerable<string> possibleDecalExportLinks) {
            var responses = new List<DecalExport>();
            var guildChannel = message.Channel as SocketGuildChannel;
            List<string> textBlobs = await LinkDownloader.DownloadAll(possibleDecalExportLinks);
            foreach (var blob in textBlobs) {
                if (string.IsNullOrWhiteSpace(blob))
                    continue;

                DecalExport decalExport = null;

                try {
                    decalExport = new DecalExport(blob);
                    if (!decalExport.DidParse)
                        continue;
                }
                catch (Exception ex) {
                    await _logger.LogAsync(new LogMessage(LogSeverity.Error, "DecalExportService", $"Error parsing DecalExport", ex));
                    continue;
                }

                if (!decalExport.HasPluginLocationsEnabled) {
                    SupportMessage errorResponse = new SupportMessage("");
                    errorResponse.AddEmbedFieldText("Errors", "Please check the locations checkbox at the top of the export window during your decal export.");
                    await message.Channel.SendMessageAsync("", false, errorResponse.Embed, null, null, new MessageReference(message.Id));
                    responses.Add(new DecalExport(blob) { Response = errorResponse });
                    continue;
                }


                var noVersionCount = decalExport.Components.Where(c => c.ComponentType != DecalComponentType.Plugin && c.NoVersion).Count();
                var errorLoadingDLLPathCount = decalExport.Components.Where(c => c.ErrorLoadingDLLPath).Count();
                var invalidComponents = decalExport.Components.Where(c => c.ComponentType != DecalComponentType.Plugin && c.Enabled == -1).Count();
                var validComponents = decalExport.Components.Where(c => c.ComponentType != DecalComponentType.Plugin && c.Enabled == 1).Count();

                var hasLatestClientVersion = decalExport.DecalSettings.ClientVersion == new Version("0.0.0.0") ? true : decalExport.DecalSettings.ClientVersion == new Version("0.0.11.6096");

                if (noVersionCount > 5 || errorLoadingDLLPathCount > 5 || invalidComponents > 5 || validComponents == 0 || !hasLatestClientVersion) {
                    SupportMessage errorResponse = new SupportMessage("");
                    errorResponse.AddEmbedFieldText("Errors", $"Your dats/client exe appear to be out of date ({decalExport.DecalSettings.ClientVersion}). Make *sure* you have updated your dats/client exe according to the install steps for [GDLE](https://www.gdleac.com/#installing) or [ACE](https://emulator.ac/how-to-play/)");
                    await message.Channel.SendMessageAsync("", false, errorResponse.Embed, null, null, new MessageReference(message.Id));
                    responses.Add(new DecalExport(blob) { Response = errorResponse });
                    continue;
                }


                SupportMessage exportResponse = new SupportMessage("");
                exportResponse.Merge(await RunGlobalDecalExportRules(message, decalExport));
                exportResponse.Merge(await RunGuildDecalExportRules(message, guildChannel.Guild.Id, decalExport));

                var link = $"[View Online](https://supportbot.utilitybelt.me/decalexport?channel={message.Channel.Id}&msg={message.Id})";

                if (exportResponse.IssueCount > 0) {
                    exportResponse.Text = $"{message.Author?.Mention} You have issues with that export that may require attention. Follow the steps below: {link}";
                    responses.Add(new DecalExport(blob) { Response = exportResponse });
                }
                else {
                    var successResponse = new SupportMessage($"That decal export looks good to me. Try the tips below if you are still having issues. {link}");
                    successResponse.AddEmbedFieldText("Tips", "Press Alt+Enter at character select to *exit* fullscreen");
                    successResponse.AddEmbedFieldText("Tips", "Make sure your client/dat files are up-to-date");
                    successResponse.AddEmbedFieldText("Tips", "Launch ThwargLauncher as administrator");
                    successResponse.AddEmbedFieldText("Tips", "Make sure the server name in thwarglauncher *exactly* matches the ingame server name");
                    successResponse.AddEmbedFieldText("Tips", "If your game closes during character creation, close thwarglauncher during creation");
                    successResponse.AddEmbedFieldText("Tips", "Close all bloatware in system tray: audio/gfx managers especially");
                    successResponse.AddEmbedFieldText("Tips", "If you have DualShock4/DualSense5 drivers installed you will need to uninstall them. [See here](https://www.reddit.com/r/AsheronsCall/comments/woorha/solved_asherons_call_black_window_not_responsive/)");
                    successResponse.AddEmbedFieldText("Tips", "Make sure you follow *all* the install steps for [GDLE](https://www.gdleac.com/#installing) or [ACE](https://emulator.ac/how-to-play/)");



                    if (decalExport.DecalVersion < DecalUpdateCheckService.LatestDecalVersion) {
                        successResponse.AddEmbedFieldText("Decal Update", $"A new version of decal is available ({DecalUpdateCheckService.LatestDecalVersion}), <https://www.decaldev.com/>");
                    }

                    if (exportResponse.EmbedFields.Count > 0) {
                        foreach (var field in exportResponse.Embed.Fields) {
                            successResponse.AddEmbedFieldText(field.Name, field.Value);
                        }
                    }
                    responses.Add(new DecalExport(blob) { Response = successResponse });
                }
            }

            return responses;
        }

        private async Task<SupportMessage> RunGuildDecalExportRules(IMessage message, ulong guildId, DecalExport decalExport) {
            if (!_modules.GuildDecalExportRuleHandlers.ContainsKey(guildId))
                return null;

            return await RunDecalExportRules(message, _modules.GuildDecalExportRuleHandlers[guildId], decalExport);
        }

        private async Task<SupportMessage> RunGlobalDecalExportRules(IMessage message, DecalExport decalExport) {
            return await RunDecalExportRules(message, _modules.GlobalDecalExportRuleHandlers, decalExport);
        }

        private async Task<SupportMessage> RunDecalExportRules(IMessage message, List<DecalExportRuleHandler> handlers, DecalExport decalExport) {
            SupportMessage response = new SupportMessage("");
            bool hasResponse = false;

            foreach (var handler in handlers) {
                try {
                    if (await RunDecalExportRuleHandler(handler, response, message, decalExport))
                        hasResponse = true;
                }
                catch (Exception ex) {
                    await _logger.LogAsync(new LogMessage(LogSeverity.Error, "ModuleService", $"Failed to run DecalExportRuleHandler {handler.Type}", ex));
                }
            }

            return hasResponse ? response : null;
        }

        private async Task<bool> RunDecalExportRuleHandler(DecalExportRuleHandler handler, SupportMessage response, IMessage message, DecalExport decalExport) {
            var moduleInstance = _services.GetService(handler.Type);
            if (moduleInstance == null)
                return false;

            Task<SupportMessage> result = (Task<SupportMessage>)handler.Handler.Invoke(moduleInstance, new object[] { message, decalExport });
            await result;

            if (result.Result != null) {
                response.Merge(result.Result);
                return true;
            }

            return false;
        }
    }
}
