﻿using Discord.Interactions;
using Discord.WebSocket;
using Discord;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHelperBot.Lib;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;

namespace ACHelperBot.Services {
    public class OpenAIGPT3Service {
        private ILogService _log;
        private InteractionService _interactions;
        private IServiceProvider _services;
        private DiscordSocketClient _client;

        public OpenAIGPT3Service(ILogService logger, InteractionService interactions, IServiceProvider services, DiscordSocketClient client) {
            _log = logger;
            _interactions = interactions;
            _services = services;
            _client = client;

            _client.Ready += Client_Ready;
            _client.MessageReceived += Client_MessageReceived;

            _log.LogAsync(new LogMessage(LogSeverity.Verbose, "OpenAIGPT3Service", $"Initialized"));
        }

        private async Task Client_Ready() {
            await Task.Run(() => {
                _log.LogAsync(new LogMessage(LogSeverity.Info, "OpenAIGPT3Service", "OpenAIGPT3Service is starting 123"));
            });
        }

        private async Task Client_MessageReceived(SocketMessage message) {
            if (message.Author.IsBot || !message.MentionedUsers.Any(u => u.IsBot && u.Username == "AC Support Bot")) {
                return;
            }
            _ = Task.Run(async () => {
                try {

                    List<string> results = new List<string>();

                    foreach (var attachment in message.Attachments) {
                        // skip files larger than 100k (my decal export is ~15k with several plugins)
                        if (attachment.Size > 1024 * 1024 * 100)
                            continue;

                        results.Add(attachment.Url);
                    }

                    List<string> textBlobs = await LinkDownloader.DownloadAll(results);


                    var query = message.Content;
                    query = Regex.Replace(query, @"<@\d+>", "");

                    if (textBlobs != null && textBlobs.Count > 0) {
                        query += $"\n{string.Join("\n", textBlobs)}";
                    }
                    query = JsonConvert.SerializeObject(query);

                    var typing = message.Channel.EnterTypingState();
                    try {
                        var guild = "dunno";
                        if (message.Channel is IGuildChannel g) {
                            guild = g.Guild?.Name;
                        }
                        Console.WriteLine($"OpenAi: Guild: {guild}  Channel:{message.Channel?.Name}");
                        var contents = await GetOpenAIResponse(query);
                        if (string.IsNullOrEmpty(contents)) {
                            throw new Exception("Empty contents from openapi server..");
                        }

                        JObject reply = JObject.Parse(contents);
                        string replyText = ((string)reply.SelectToken("choices[0].text")).Trim();
                        if (!string.IsNullOrEmpty(replyText)) {
                            Console.WriteLine($"ReplyText: {replyText}");
                            var res = SplitByNewLines(replyText);
                            foreach (var r in res) {
                                await message.Channel.SendMessageAsync(r, false, null, null, null, new MessageReference(message.Id));
                            }
                        }
                        else {
                            await message.Channel.SendMessageAsync("I got nothing (no api response)", false, null, null, null, new MessageReference(message.Id));
                        }
                    }
                    catch (Exception ex) {
                        Console.WriteLine(ex);
                    }
                    finally {
                        typing.Dispose();
                    }

                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                    await message.Channel.SendMessageAsync("I got nothing (no api response)", false, null, null, null, new MessageReference(message.Id));
                }
            });

        }
        public static List<string> SplitByNewLines(string input) {
            List<string> result = new List<string>();
            string[] lines = input.Split('\n');

            string buffer = "";
            foreach (string line in lines) {
                if (buffer.Length + line.Length < 2000) {
                    buffer += $"{line}\n";
                }
                else {
                    result.Add(buffer);
                    buffer = $"{line}\n";
                }
            }

            if (!string.IsNullOrEmpty(buffer)) {
                result.Add(buffer);
            }

            return result;
        }

        private async Task<string> GetOpenAIResponse(string query) {
            try {
                string contents = null;
                await Task.Run(() => {
                    using (WebClient webClient = new WebClient()) {
                        var json = $"{{\r\n  \"model\": \"text-davinci-003\",\r\n  \"prompt\": {query},\r\n  \"temperature\": 0.0,\r\n  \"max_tokens\": 2048,\r\n  \"frequency_penalty\": 0,\r\n  \"presence_penalty\": 0,\r\n  \"best_of\": 1\r\n}}";
                        Console.WriteLine(json);
                        webClient.Headers.Add($"Authorization: Bearer {System.Environment.GetEnvironmentVariable("OPENAI_SECRET_KEY")}");
                        webClient.Headers.Add("Content-Type: application/json");
                        contents = webClient.UploadString("https://api.openai.com/v1/completions", "POST", json);
                    }
                });
                return contents;
            }
            catch (Exception ex) {
                Console.WriteLine($"Failed to download:");
                Console.WriteLine(ex.ToString());
            }

            return null;
        }
    }
}
