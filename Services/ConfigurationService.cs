﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Services {
    class ConfigurationService : IConfiguration {
        private CancellationChangeToken _token;
        private Dictionary<string, string> _config;

        public string this[string key] {
            get => _config.TryGetValue(key, out string result) ? result : null;
            set {
                if (_config.ContainsKey(key))
                    _config[key] = value;
                else
                    _config.TryAdd(key, value);
            }
        }

        public ConfigurationService() {
            _token = new CancellationChangeToken(new System.Threading.CancellationToken());
            _config = new Dictionary<string, string>() {
                { "DISCORD_TOKEN", Environment.GetEnvironmentVariable("ACHB_DISCORD_TOKEN") },
                { "OCR_KEY", Environment.GetEnvironmentVariable("ACHB_OCR_KEY") },
                { "GIT_VERSION", Environment.GetEnvironmentVariable("GIT_VERSION") },
                { "JSDOODLE_ID", Environment.GetEnvironmentVariable("ACHB_JSDOODLE_ID") },
                { "JSDOODLE_SECRET", Environment.GetEnvironmentVariable("ACHB_JSDOODLE_SECRET") },
            };
        }

        public IEnumerable<IConfigurationSection> GetChildren() {
            return new List<IConfigurationSection>();
        }

        public IChangeToken GetReloadToken() {
            return _token;
        }

        public IConfigurationSection GetSection(string key) {
            return null;
        }
    }
}
