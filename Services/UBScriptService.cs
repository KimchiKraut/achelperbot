﻿using Discord.Interactions;
using Discord.WebSocket;
using Discord;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHelperBot.Lib;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections.Concurrent;
using Discord.Commands;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Net;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ACHelperBot.Services {
    public class UBScriptService {
        private ILogService _log;
        private InteractionService _interactions;
        private IServiceProvider _services;
        private DiscordSocketClient _client;
        private IConfiguration _config;
        private ScriptsController scripts;
        private bool scriptsReady = false;

        private static uint NextId = 1;
        private static Regex CodeBlockRe = new Regex(@"\`\`\`(?<lang>\w*)\s+(?<code>.*?)\`\`\`", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private struct LangData {
            public int ApiVersion;
            public List<string> Aliases;

            public LangData(int apiVersion, List<string> aliases = null) {
                if (aliases == null)
                    aliases = new List<string>();

                ApiVersion = apiVersion;
                Aliases = aliases;
            }
        }

        private static Dictionary<string, LangData> jdoodleLanguages = new Dictionary<string, LangData>() {
            { "java", new LangData(4) },
            { "c", new LangData(5) },
            { "c99", new LangData(4) },
            { "cpp", new LangData(5) },
            { "cpp14", new LangData(4) },
            { "cpp17", new LangData(1) },
            { "php", new LangData(4) },
            { "perl", new LangData(4) },
            { "python2", new LangData(3) },
            { "python3", new LangData(4, new List<string>(){ "python", "py" }) },
            { "ruby", new LangData(4) },
            { "go", new LangData(4) },
            { "scala", new LangData(4) },
            { "bash", new LangData(4) },
            { "sql", new LangData(4) },
            { "pascal", new LangData(3) },
            { "csharp", new LangData(4, new List<string>(){ "cs" }) },
            { "vbn", new LangData(4) },
            { "haskell", new LangData(4) },
            { "objc", new LangData(4) },
            { "swift", new LangData(4) },
            { "groovy", new LangData(4) },
            { "fortran", new LangData(4) },
            { "brainfuck", new LangData(0) },
            { "lua", new LangData(3) },
            { "tcl", new LangData(4) },
            { "hack", new LangData(0) },
            { "rust", new LangData(4) },
            { "d", new LangData(2) },
            { "ada", new LangData(4) },
            { "r", new LangData(4) },
            { "freebasic", new LangData(2) },
            { "verilog", new LangData(3) },
            { "cobol", new LangData(3) },
            { "dart", new LangData(4) },
            { "yabasic", new LangData(1) },
            { "clojure", new LangData(3) },
            { "nodejs", new LangData(4, new List<string>(){ "js", "javascript" }) },
            { "scheme", new LangData(3) },
            { "forth", new LangData(0) },
            { "prolog", new LangData(2) },
            { "octave", new LangData(4) },
            { "coffeescript", new LangData(4) },
            { "icon", new LangData(1) },
            { "fsharp", new LangData(1) },
            { "gccasm", new LangData(3) },
            { "intercal", new LangData(0) },
            { "nemerle", new LangData(0) },
            { "ocaml", new LangData(2) },
            { "unlambda", new LangData(1) },
            { "picolisp", new LangData(4) },
            { "spidermonkey", new LangData(1) },
            { "rhino", new LangData(2) },
            { "bc", new LangData(1) },
            { "clisp", new LangData(3) },
            { "elixir", new LangData(4) },
            { "factor", new LangData(3) },
            { "falcon", new LangData(0) },
            { "fantom", new LangData(0) },
            { "nim", new LangData(3) },
            { "pike", new LangData(1) },
            { "smalltalk", new LangData(0) },
            { "mozart", new LangData(0) },
            { "lolcode", new LangData(0) },
            { "racket", new LangData(3) },
            { "kotlin", new LangData(3) },
            { "whitespace", new LangData(0) },
            { "erlang", new LangData(1) },
            { "jlang", new LangData(0) },
            { "haxe", new LangData(0) },
            { "fasm", new LangData(0) },
            { "awk", new LangData(0) },
            { "algol", new LangData(0) },
            { "befunge", new LangData(0) },
        };

        private class ScriptResult {
            public uint Id { get; set; }
            public string Lang { get; set; }
            public string Code { get; set; }
            public bool DidError { get; set; }
            public string Output { get; set; }
            public bool DidRun { get; set; }
            public bool TimedOut { get; internal set; }
        }

        public UBScriptService(ILogService logger, InteractionService interactions, IServiceProvider services, DiscordSocketClient client, IConfiguration config) {
            _log = logger;
            _interactions = interactions;
            _services = services;
            _client = client;
            _config = config;

            _client.Ready += Client_Ready;
            _client.MessageReceived += Client_MessageReceived;
            _client.MessageUpdated += _client_MessageUpdated;
            _client.MessageDeleted += _client_MessageDeleted;

            _log.LogAsync(new LogMessage(LogSeverity.Verbose, "UBScriptService", $"Initialized"));
        }

        private Task _client_MessageDeleted(Cacheable<IMessage, ulong> arg1, Cacheable<IMessageChannel, ulong> arg2) {
            if (MessageIsInScriptableChannel(arg2.Value)) {

                _ = Task.Run(async () => {
                    var messages = await arg2.Value.GetMessagesAsync(100).FlattenAsync();

                    foreach (var possibleRefMessage in messages) {
                        if (possibleRefMessage.Author.Id == _client.CurrentUser.Id && possibleRefMessage.Reference.MessageId.Value == arg1.Id) {
                            await possibleRefMessage.DeleteAsync();

                            break;
                        }
                    }
                });
            }

            return Task.CompletedTask;
        }

        private bool MessageIsInScriptableChannel(SocketMessage message) {
            if (message.Author.IsBot || !message.MentionedUsers.Any(u => u.IsBot && u.Username == "AC Support Bot")) {
                return false;
            }

            return MessageIsInScriptableChannel(message.Channel);
        }

        private bool MessageIsInScriptableChannel(IMessageChannel channel) {
            return true; // (channel.Name == "test" || channel.Name == "utilitybelt");
        }

        private Task _client_MessageUpdated(Cacheable<IMessage, ulong> arg1, SocketMessage message, ISocketMessageChannel arg3) {
            if (message != null && !string.IsNullOrEmpty(message.ToString()) && MessageIsInScriptableChannel(message) && CodeBlockRe.IsMatch(message.ToString())) {
                _ = Task.Run(async () => {
                    var messages = await message.Channel.GetMessagesAsync(100).FlattenAsync();
                    bool foundResponse = false;
                    foreach (var possibleRefMessage in messages) {
                        if (possibleRefMessage.Author.Id == _client.CurrentUser.Id && possibleRefMessage.Reference.MessageId.Value == message.Id) {
                            foundResponse = true;
                            RunMessageScripts(message, (IUserMessage)possibleRefMessage);

                            break;
                        }
                    }

                    if (!foundResponse) {
                        RunMessageScripts(message, null);
                    }
                });
            }

            return Task.CompletedTask;
        }

        private async Task Client_MessageReceived(SocketMessage message) {
            if (!scriptsReady) {
                Console.WriteLine("scripts not ready....");
                return;
            }

            if (MessageIsInScriptableChannel(message) && CodeBlockRe.IsMatch(message.ToString())) {
                RunMessageScripts(message, null);
            }
        }

        private bool TryGetLangData(string lang, out LangData data, out string foundLang) {
            lang = lang.ToLower();
            if (jdoodleLanguages.ContainsKey(lang)) {
                data = jdoodleLanguages[lang];
                foundLang = lang;
                return true;
            }

            foreach (var jLang in jdoodleLanguages) {
                if (jLang.Value.Aliases.Contains(lang)) {
                    data = jLang.Value;
                    foundLang = jLang.Key;
                    return true;
                }
            }

            data = new LangData();
            foundLang = "";

            return false;
        }

        private void RunMessageScripts(IMessage message, IUserMessage responseMessage) {
            var matches = CodeBlockRe.Matches(message.ToString());

            if (!matches.Any(m => TryGetLangData(m.Groups["lang"].Value, out LangData r, out string l))) {
                return;
            }

            var fieldOutputs = new ConcurrentDictionary<uint, ScriptResult>();

            Action resetFieldOutputs = () => {
                fieldOutputs.Clear();
                for (var i = 0; i < matches.Count; i++) {
                    if (TryGetLangData(matches[i].Groups["lang"].Value, out LangData r, out string lang)) {
                        var id = (uint)(NextId++);
                        //msg.AppendLine($"Script DiscordPaste{currentId + i} Output:");
                        fieldOutputs.TryAdd(id, new ScriptResult() {
                            Id = id,
                            Lang = lang,
                            Code = matches[i].Groups["code"].Value,
                            DidRun = false,
                            Output = "Running..."
                        });
                    }
                }
            };
            resetFieldOutputs();
            Func<Embed> buildEmbed = () => {
                EmbedBuilder builder = new EmbedBuilder();
                builder.Description = "";

                foreach (var kv in fieldOutputs) {
                    builder.AddField((fieldBuilder) => {
                        fieldBuilder.IsInline = false;
                        fieldBuilder.Name = $"{kv.Value.Lang} DiscordPaste{kv.Key} Output:";
                        fieldBuilder.Value = $"```\n{kv.Value.Output}\n```";
                    });
                }
                if (fieldOutputs.Any(kv => kv.Value.DidError)) {
                    builder.Color = Color.Red;
                }
                else if (fieldOutputs.Any(kv => kv.Value.TimedOut)) {
                    builder.Color = Color.LightOrange;
                }
                else if (fieldOutputs.Any(kv => !kv.Value.DidRun)) {
                    builder.Color = Color.Blue;
                }
                else {
                    builder.Color = Color.Green;
                }

                return builder.Build();
            };


            _ = Task.Run(async () => {
                var body = $"Running {matches.Count} script{(matches.Count > 1 ? "s" : "")}...";

                if (responseMessage == null) {
                    responseMessage = await message.Channel.SendMessageAsync(body, false, buildEmbed(), null, null, new MessageReference(message.Id));
                }
                else {
                    await (responseMessage).ModifyAsync(x => {
                        x.Content = body;
                        x.Embed = buildEmbed();
                    });
                }

                List<Task> TaskList = new List<Task>();
                foreach (var key in fieldOutputs.Keys.ToList()) {
                    var LastTask = new Task(async () => {
                        var output = fieldOutputs[key];

                        if (fieldOutputs.TryRemove(key, out ScriptResult value)) {
                            if (value.Lang.ToLower().Equals("lua")) {
                                await RunScriptAsync(key, output.Code, value);
                            }
                            else {
                                await RunJDoodleScriptAsync(key, output.Code, value);
                            }
                            fieldOutputs.TryAdd(key, value);
                        }

                        await (responseMessage).ModifyAsync(x => {
                            x.Embed = buildEmbed();
                        });
                    });
                    LastTask.Start();
                    TaskList.Add(LastTask);
                }

                Task.WaitAll(TaskList.ToArray());
            });
        }

        private async Task RunJDoodleScriptAsync(uint nextId, string code, ScriptResult result) {
            try {
                string contents = null;
                await Task.Run(() => {
                    try {
                        using (WebClient webClient = new WebClient()) {
                            code = JsonConvert.SerializeObject(code);
                            var json = $"{{ \"clientId\": \"{_config["JSDOODLE_ID"]}\", \"clientSecret\": \"{_config["JSDOODLE_SECRET"]}\", \"script\": {code}, \"stdin\": \"\", \"language\": \"{result.Lang}\", \"version\": {jdoodleLanguages[result.Lang.ToLower()].ApiVersion} }}";
                            webClient.Headers.Add($"Authorization: Bearer {System.Environment.GetEnvironmentVariable("OPENAI_SECRET_KEY")}");
                            webClient.Headers.Add("Content-Type: application/json");
                            Console.WriteLine(json);
                            contents = webClient.UploadString("https://api.jdoodle.com/v1/execute", "POST", json);

                            Console.WriteLine($"RESPONSE: {contents}");

                            JObject reply = JObject.Parse(contents);

                            if (reply.ContainsKey("error")) {
                                result.Output = ((string)reply.SelectToken("error")).Trim();
                                result.DidError = true;
                            }
                            else if (reply.ContainsKey("output")) {
                                result.Output = ((string)reply.SelectToken("output")).Trim();
                            }
                            else {
                                result.Output = "API Error...";
                                result.DidError = true;
                            }

                            result.Output = TrimLogs(result.Output.Split("\n").ToList());
                        }
                    }
                    catch (Exception ex) {
                        Console.WriteLine($"Failed to download:");
                        Console.WriteLine(ex.ToString());
                        result.Output = "Error...";
                        result.DidError = true;
                    }
                });
            }
            catch (Exception ex) {
                Console.WriteLine($"Failed to download:");
                Console.WriteLine(ex.ToString());
            }
        }

        private async Task RunScriptAsync(uint nextId, string code, ScriptResult result) {
            var fieldName = $"DiscordPaste{NextId} Output:";
            var id = $"DiscordPaste{NextId}";
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "UBScriptService", $"Match: {code}"));

            if (scripts.Manager.GetScript(id) != null) {
                scripts.Manager.StopScript(id);
            }

            var logs = new List<string>();
            bool didError = false;

            await scripts.Manager.StartScript(id, false);
            var script = scripts.Manager.GetScript(id);

            script.OnLogText += (s, e) => {
                logs.Add(e.Text);
            };

            var watch = new Stopwatch();
            watch.Start();

            int timeout = (int)TimeSpan.FromSeconds(5).TotalMilliseconds;
            var task = script.RunTextAsync(code);

            if (await Task.WhenAny(task, Task.Delay(timeout)) == task) {
                string output = TrimLogs(logs);
                result.DidError = didError;
                result.TimedOut = false;
                watch.Stop();
                result.Output = output + $"\nResult: {PrettyPrint(task.Result)} ({Math.Round(((double)watch.ElapsedTicks / Stopwatch.Frequency) * 1000.0, 3)}ms)";
            }
            else {
                string output = TrimLogs(logs);
                result.DidError = didError;
                result.TimedOut = true;
                result.Output = output + $"\nResult: Timed out after {Math.Round(timeout / 1000.0, 0):N0} seconds!";
            }

            scripts.Manager.StopScript(id);
        }

        private string TrimLogs(List<string> logs) {
            var output = "";
            var MAX_LEN = 800;

            if (logs.Count > 14) {
                var firstLines = new List<string>();
                var lastLines = new List<string>();

                var totalStrLen = 0;
                var maxFirstStrLen = MAX_LEN / 2;
                var maxLastStrLen = MAX_LEN;
                for (var i = 0; i < 6; i++) {
                    if (totalStrLen + logs[i].Length < maxFirstStrLen) {
                        firstLines.Add(logs[i]);
                    }
                    else if (totalStrLen < maxFirstStrLen) {
                        firstLines.Add(logs[i].Substring(0, maxFirstStrLen - totalStrLen) + "...");
                        break;
                    }
                    else {
                        break;
                    }
                }

                for (var i = 0; i < 7; i++) {
                    var line = logs[logs.Count - i - 1];
                    if (totalStrLen + line.Length < maxLastStrLen) {
                        lastLines.Insert(0, line);
                    }
                    else if (totalStrLen < maxLastStrLen) {
                        lastLines.Insert(0, "..." + line.Substring(line.Length - maxLastStrLen - totalStrLen, maxLastStrLen - totalStrLen));
                        break;
                    }
                    else {
                        break;
                    }
                }

                output = String.Join("\n", firstLines) + $"\n... [truncated {logs.Count - (firstLines.Count + lastLines.Count)} lines] ...\n" + String.Join("\n", lastLines);
            }
            else if (logs.Sum(l => l.Length) >= MAX_LEN) {
                var logText = string.Join("\n", logs);
                output = "..." + logText.Substring(logText.Length - MAX_LEN, MAX_LEN);
            }
            else {
                output = string.Join("\n", logs);
            }

            return output;
        }

        public static string PrettyPrint(object res) {
            if (res == null)
                return "null";

            return $"({res.GetType().Name}) {res}";
        }

        private async Task Client_Ready() {
            await Task.Run(() => {
                _log.LogAsync(new LogMessage(LogSeverity.Info, "UBScriptService", "ScriptsController is starting"));
                scripts = new ScriptsController();
                scriptsReady = true;
                _log.LogAsync(new LogMessage(LogSeverity.Info, "UBScriptService", "ScriptsController is ready"));
            });
        }
    }
}
