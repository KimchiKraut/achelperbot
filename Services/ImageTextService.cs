﻿using ACHelperBot.Models;
using Discord;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Services {
    public class ImageTextService {
        private static Regex URLRegex = new Regex(@"(https?:\/\/)?([\w\-])+\.{1}([a-zA-Z]{2,63})([\/\w-]*)*\/?\??([^#\n\r]*)?#?([^\n\r]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private readonly ILogService _logger;
        private IConfiguration _config;

        public ImageTextService(ILogService logger, IConfiguration config) {
            _logger = logger;
            _config = config;
        }

        public async Task<string> GetAnyImageText(IMessage message) {
            var foundText = "";
            var imageAttachments = FindImageAttachments(message);
            var imageLinks = FindImageLinks(message);
            var possibleImages = imageAttachments.Concat(imageLinks);

            if (possibleImages.Count() > 0) {
                foundText = await DownloadAndCheckImages(message, possibleImages.Distinct());
            }

            if (string.IsNullOrEmpty(foundText))
                return null;

            await _logger.LogAsync(new LogMessage(LogSeverity.Error, "ImageTextService", $"Found Image Text: {foundText}"));

            return foundText;
        }

        internal List<string> FindImageAttachments(IMessage message) {
            List<string> results = new List<string>();

            foreach (var attachment in message.Attachments) {
                // 1mb limit for ocr api
                if (attachment.Size > 1024 * 1024)
                    continue;

                if (attachment.Filename.EndsWith(".png") || attachment.Filename.EndsWith(".jpg") || attachment.Filename.EndsWith(".gif")) {
                    results.Add(attachment.Url);
                }
            }

            return results;
        }

        internal List<string> FindImageLinks(IMessage message) {
            List<string> results = new List<string>();
            MatchCollection matches = URLRegex.Matches(message.Content.ToString());
            foreach (Match match in matches) {
                try {
                    Uri foundUri = new Uri(match.Value);

                    if (match.Value.EndsWith(".png") || match.Value.EndsWith(".jpg") || match.Value.EndsWith(".gif")) {
                        results.Add(match.Value);
                    }
                    else if (foundUri.Host.ToLower() == "imgur.com") {
                        results.Add($"https://i.imgur.com{foundUri.AbsolutePath}.jpg");
                    }
                }
                catch { }
            }

            return results;
        }

        private async Task<string> DownloadAndCheckImages(IMessage arg, IEnumerable<string> imageAttachments) {
            var results = "";
            //todo: async foreach for multiple images?
            foreach (var image in imageAttachments) {
                try {
                    HttpClient httpClient = new HttpClient();
                    httpClient.Timeout = new TimeSpan(1, 1, 1);

                    MultipartFormDataContent form = new MultipartFormDataContent();
                    form.Add(new StringContent(_config["OCR_KEY"]), "apikey");
                    form.Add(new StringContent("eng"), "language");
                    form.Add(new StringContent(image), "url");

                    HttpResponseMessage response = await httpClient.PostAsync("https://api.ocr.space/Parse/Image", form);
                    string strContent = await response.Content.ReadAsStringAsync();
                    Rootobject ocrResult = JsonConvert.DeserializeObject<Rootobject>(strContent);

                    var text = "";
                    if (ocrResult.OCRExitCode == 1) {
                        for (int i = 0; i < ocrResult.ParsedResults.Count(); i++) {
                            text += ocrResult.ParsedResults[i].ParsedText;
                        }
                        text = text.Trim().Replace("\n", "").Replace("\r", " ");
                    }
                    else {
                        await _logger.LogAsync(new LogMessage(LogSeverity.Error, "ImageTextService", strContent));
                    }

                    if (!string.IsNullOrEmpty(text)) {
                        results += text;
                    }
                }
                catch (Exception ex) {
                    await _logger.LogAsync(new LogMessage(LogSeverity.Error, "ImageTextService", $"Could not download image: {image}", ex));
                }
            }

            return results;
        }
    }
}
