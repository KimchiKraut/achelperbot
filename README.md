### About
ACHelperBot is a discord bot that actively monitors chat for decal exports, common error text, and error screenshots and reports common issues/solutions.

### Features
- Automatically detects decal exports from file uploads/pastebin links.
- Parses decal exports for common errors/warnings.
- Searches chat messages for error text
- Searches image attachments/links for error text (ocr using [ocr.space](https://ocr.space/OCRAPI))
- Adds context menus and slash command for providing decal export instructions to users

### Add the bot to your server
Add the bot to a discord channel you manage by clicking [here](https://discord.com/api/oauth2/authorize?client_id=706191103669567508&permissions=274877925376&scope=bot%20applications.commands).

### Host your own bot
If you want to host the bot on your own server, you need to: 
- Create a new Discord Application via the [Discord Developers Portal](https://discordapp.com/developers/applications)
- Set environment variable `ACHB_DISCORD_TOKEN` to your bots token
- Set environment variable `ACHB_OCR_KEY` to key from [ocr.space](https://ocr.space/OCRAPI)
- Generate a OAuth2 URL on the Discord Application OAuth2 page to add the bot to your server
- Build and run the bot

### Issues Detected
- Decal Exports
	- Check Decal version
	- Check Export Locations enabled
	- Check Decal components enabled (surrogates/filters/services)
	- Check for plugin/filter NoDLL errors
	- Check valid Launcher App
	- Check decal injection method
	- Check for required decal components
	- Check for files in temporary locations (downloads folder)
	- Check for non-default install locations
	- ThwargFilter
		- Check properly registered
	- UtilityBelt (if installed)
		- Check latest version
		- Check VTank / VVS installed
		- Check .NET 3.5 installed
	- GoArrow (if installed)
		- Detect conflict if both original and VVS version are installed
	- LifeTank X
		- Suggest removing and replacing with vtank
- Chat Triggers (+ Image OCR Text)
	- directx errors
	- missing vcredist 2005
	- vtank missing gameinfodb.ugd
	- client out of memory
	- VTank not fully logged in
	- client closes during character creation
	- client unable to open dat files
	- vtank install missing ICSharpCode.SharpZipLib

## Adding Export Rules
See `Modules/DecalExport/*` for example export rules

## Custom Server Responses
If you want custom responses / commands for your server, check out `Modules/Guilds/*`

### Thanks
- Thanks to Hells for a lot of the check logic

### Planned Features
- More error/warning triggers

